#!/usr/bin/env bash

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

cd "$SCRIPT_DIR/.." || exit
curl -d '{ 
  "wsuID": "1234567",
  "resident": true,
  "zipCode": "01602",
  "unemployment": false,
  "assistance": {
    "socSec": false,
    "TANF": false,
    "finAid": false,
    "other": false,
    "SNAP": false,
    "WIC": false,
    "breakfast": false,
    "lunch": false,
    "SFSP": false
  },
  "guestAge": 42,
  "numberInHousehold": 4
}' -H "Content-Type: application/json" -X POST http://localhost:10350/guests

curl -d '{ 
  "wsuID": "1234568",
  "resident": false,
  "zipCode": "01603",
  "unemployment": false,
  "assistance": {
    "socSec": false,
    "TANF": false,
    "finAid": false,
    "other": false,
    "SNAP": false,
    "WIC": false,
    "breakfast": false,
    "lunch": false,
    "SFSP": false
  },
  "guestAge": 42, 
  "numberInHousehold": 4
}' -H "Content-Type: application/json" -X POST http://localhost:10350/guests
